#include "keyboard_visualisation.h"
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

keyboard_visu *create_keyboard_visu(WINDOW *win, int y, int x, int size, char* layout)
{
    keyboard_visu* kb_visu = (keyboard_visu*) malloc(sizeof(keyboard_visu));

    kb_visu->win = win;
    kb_visu->x = x;
    kb_visu->y = y;
    kb_visu->size = size;
    kb_visu->layout = layout;
    kb_visu->mark = 0;
    kb_visu->mark_alt = 0;
    kb_visu->upper = 0;
    return kb_visu;
}

void draw_kb(keyboard_visu* kb_visu, int next_char, int typed)
{
        wclear(kb_visu->win);
        mark_key(kb_visu, next_char, 0);
        mark_key(kb_visu, typed, 1);
        draw_keys(kb_visu);
        box(kb_visu->win,'|','.');
}

void mark_key(keyboard_visu* kb_visu, char key, int alt)
{
    if(alt)
    {
        kb_visu->mark_alt = tolower(key);
    }else
    {
        kb_visu->mark = tolower(key);
        kb_visu->upper = (kb_visu->mark == key) ? 0 : 1;
    }
}

void draw_keys(keyboard_visu* kb_visu)
{
    int i;
    int column;
    int row;
    int offset = 0;
    int len = strlen(kb_visu->layout);
    for(i=0;i<len;i++)
    {
        column = (i-1)%12;
        row = (i-1)/12 + 1;
        switch(row)
        {
            case 2:
            case 3:
                offset = row;
                break;
            case 4:
                offset = 1;
                if(column == 0 || column == 12)
                {
                    if(kb_visu->upper)
                        wattron(kb_visu->win,A_REVERSE | COLOR_PAIR(1));
                    mvwprintw(kb_visu->win, kb_visu->y+row, kb_visu->x+-1*kb_visu->size, "⇧");
                    wattroff(kb_visu->win, A_REVERSE | COLOR_PAIR(1));
                }

                break;
        }
        if(kb_visu->mark == kb_visu->layout[i] && kb_visu->mark_alt != kb_visu->mark)
        {
            wattron(kb_visu->win,A_REVERSE | COLOR_PAIR(1));
        }else if(kb_visu->mark == kb_visu->layout[i])
        {
            wattron(kb_visu->win,A_REVERSE | COLOR_PAIR(2));
        }else if(kb_visu->mark_alt == kb_visu->layout[i])
        {
            wattron(kb_visu->win,A_REVERSE | COLOR_PAIR(3));
        }

        mvwaddch(kb_visu->win, kb_visu->y+row, kb_visu->x+offset+column*kb_visu->size, kb_visu->layout[i]);
        wattroff(kb_visu->win, A_REVERSE | COLOR_PAIR(1));
    }
}

void destroy_keyboard_visu(keyboard_visu *kb_visu)
{
    free(kb_visu);
}
