#pragma once
#include <ncurses.h>

typedef struct
{
    WINDOW *win;
    int x;
    int y;
    int size;
    char mark;
    char mark_alt;
    char* layout;
    int upper;
} keyboard_visu;

keyboard_visu *create_keyboard_visu(WINDOW *win, int y, int x, int size, char* layout);
void mark_key(keyboard_visu* kb_visu, char key, int alternate);
void draw_keys(keyboard_visu* kb_visu);
void draw_kb(keyboard_visu* kb_visu, int next_char, int typed);
void destroy_keyboard_visu(keyboard_visu*);
