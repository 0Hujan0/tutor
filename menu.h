#pragma once
#include <ncurses.h>

typedef struct
{
    WINDOW* win;
    int x;
    int y;
    char** entries;
    int numEntries;
} menu;

menu *create_menu(WINDOW* win, int y, int x, char** entries, int numEntries);
void destroy_menu(menu* men);
