#include "input_text.h"
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

input_text *create_input(WINDOW* win, char* text)
{
    input_text* inp_text = (input_text*) malloc(sizeof(input_text));
    inp_text->text=text;
    inp_text->len=strlen(text);
    inp_text->rest = inp_text->len;
    inp_text->window=win;
    inp_text->margin_y=1;
    inp_text->margin_x=1;
    inp_text->cursor=0;
    inp_text->started=0;
    inp_text->mode = MOVE_CURSOR;
    return inp_text;
}

int enter_char(input_text* text, int input)
{
    if(text->cursor >= text->len)
        return 1;
    if(input == text->text[text->cursor])
    {
        if(!text->started)
        {
            text->started = 1;
            gettimeofday(&text->before, NULL);
        }
        text->cursor++;
        text->rest--;
    }
    if (text->rest <= 0)
    {
        stop(text);
        return 1;
    }
    return 0;
}

void stop(input_text* text)
{
        text->len -= text->rest;
        gettimeofday(&text->after, NULL);
}

char next_char(input_text* text)
{
    return text->text[text->cursor];
}

void draw_input(input_text* text)
{
        wclear(text->window);
        print_text(text);
        box(text->window,'|','.');
        wrefresh(text->window);
}

double get_char_per_minute(input_text* text)
{
    if(!text->started)
        return 0.;
    double minutes = ((text->after.tv_usec - text->before.tv_usec) / 1000000.0 + (text->after.tv_sec - text->before.tv_sec)) / 60 ;
    return text->len / minutes;
}
double get_words_per_minute(input_text* text)
{
    if(!text->started)
        return 0.;
    return get_char_per_minute(text) / 5;
}

void print_text(input_text* text)
{
    int i;
    int y = text->margin_y;
    int x = text->margin_x;
    switch (text->mode)
    {
        case MOVE_TEXT:
            i = text->cursor;
            wattron(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(1));
            mvwaddch(text->window, y, x,text->text[i]);
            wattroff(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(1));
            for(i++; i<text->len; i++)
            {
                if ( ++x >= getmaxx(text->window) - text->margin_x )
                {
                    if ( ++y >= getmaxy(text->window) - text->margin_y )
                    {
                        break;
                    }
                    x = text->margin_x;
                }
                mvwaddch(text->window, y, x,text->text[i]);
            }
            break;
        case MOVE_CURSOR:
            for(i = 0; i<text->len; i++)
            {
                if ( ++x >= getmaxx(text->window) - text->margin_x )
                {
                    if ( ++y >= getmaxy(text->window) - text->margin_y )
                    {
                        break;
                    }
                    x = text->margin_x;
                }
                if ( i < text->cursor )
                {
                    wattron(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(0));
                    mvwaddch(text->window, y, x,text->text[i]);
                    wattroff(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(0));
                }
                else if ( i == text->cursor )
                {
                    wattron(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(1));
                    mvwaddch(text->window, y, x,text->text[i]);
                    wattroff(text->window,A_BOLD|A_REVERSE|COLOR_PAIR(1));
                }
                else
                {
                    mvwaddch(text->window, y, x,text->text[i]);
                }
            }

            break;
    }
}

void delete_input(input_text* text)
{
    free(text);
}
