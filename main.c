#include <ncurses.h>
#include <string.h>
#include <sys/time.h>
#include <locale.h>
#include <signal.h>
#include "input_text.h"
#include "keyboard_visualisation.h"

void resize_windows(WINDOW * kb_win, WINDOW * text_win)
{
    mvwin(kb_win, LINES - 6, 0);
    wresize(kb_win, 6, COLS);
    wresize(text_win, LINES - 6, COLS);
}

int main(int argc,char* argv[])
{
    char* sentence = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla ac diam sit amet lorem rutrum rhoncus. Nulla facilisi. Cras sed malesuada neque, vel dictum neque. Fusce at lacinia lectus. Integer in imperdiet tellus. In rhoncus lacus a tempor tincidunt. Aliquam augue tellus, rhoncus ut erat nec, pulvinar venenatis quam.";

    char* layout = "`1234567890-=qwertyuiop[]asdfghjkl;'#\\zxcvbnm,./";
    setlocale(LC_ALL, "");
    WINDOW *w = initscr();
    WINDOW *keyboard_win = newwin(6, COLS, LINES - 6, 0);
    WINDOW *input_win = newwin(LINES - 6, COLS, 0, 0);
    curs_set(0);
    noecho();
    cbreak();
    keypad(w, 1);
    timeout(500);
    start_color();
    init_pair(1, COLOR_RED, COLOR_BLACK);
    init_pair(2, COLOR_RED, COLOR_BLUE);
    init_pair(3, COLOR_BLACK, COLOR_BLUE);

    keyboard_visu *kb_visu = create_keyboard_visu(keyboard_win, 0, 10, 3, layout);
    input_text* text= create_input(input_win, sentence);
    int character=-1;
    do
    {
        clear();
        refresh();
        resize_windows(keyboard_win, input_win);
        draw_input(text);
        draw_kb(kb_visu, next_char(text), character);
        wrefresh(keyboard_win);
        character = wgetch(w);

        if (character == KEY_F(10))
        {
            stop(text);
            break;
        }
        if (character == KEY_F(1))
        {
            switch( text->mode)
            {
                case MOVE_TEXT:
                    text->mode = MOVE_CURSOR;
                    break;
                case MOVE_CURSOR:
                    text->mode = MOVE_TEXT;
                    break;
            }
        }
    }while(!enter_char(text, character));

    clear();
    mvprintw(2,2,"Speed : %.2f Characters per minute", get_char_per_minute(text)) / 60 ;
    mvprintw(3,2,"Speed : %.2f Word per minute", get_words_per_minute(text));
    timeout(-1);
    getch();
    delete_input(text);
    text=NULL;
    endwin();
}
