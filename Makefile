all: input_text keyboard_visu main.c
	gcc main.c input_text.o keyboard_visualisation.o -o main -lncursesw
run:
	./main
input_text: input_text.c
	gcc -c input_text.c
keyboard_visu: keyboard_visualisation.c
	gcc -c keyboard_visualisation.c
clean:
	rm *.o main
