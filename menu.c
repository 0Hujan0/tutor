#include "menu.h"
#include <ncurses.h>
#include <stdlib.h>

menu *create_menu(WINDOW* win, int y, int x, char** entries, int numEntries)
{
    menu *men = (menu *) malloc(sizeof(menu));
    men->win = win;
    men->y = y;
    men->x = x;
    men->entries = entries;
    men->numEntries = numEntries;
    return men;
}

void destroy_menu(menu* men)
{
    free(men);
}
