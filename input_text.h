#pragma once
#include <ncurses.h>
#include <sys/time.h>

typedef enum
{
    MOVE_TEXT,
    MOVE_CURSOR
} display_mode;

typedef struct
{
    char *text;
    int len;
    int rest;
    WINDOW *window;
    int margin_y;
    int margin_x;
    int cursor;
    int started;
    display_mode mode;
    struct timeval before;
    struct timeval after;
} input_text;

input_text *create_input(WINDOW* win, char* text);
int enter_char(input_text* text, int input);
char next_char(input_text* text);
void stop(input_text*);
void draw_input(input_text*);
double get_char_per_minute(input_text*);
double get_words_per_minute(input_text*);
void print_text(input_text*);
void delete_input(input_text*);
